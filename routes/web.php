<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('vk')->group(function () {
    Route::prefix('{code}')->group(function () {
        Route::post('handle', 'MainController@handle');
    });
});
